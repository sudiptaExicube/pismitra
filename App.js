import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';

const YourApp = () => {
  return (
     <WebView source={{ uri: 'https://pismitra.in/portallogin/login' }} />
  );
};

export default YourApp;